
## 不具合

- 背景の表示がずれる
    - renderモジュールを参照実装に入れ替えてみたが壊れたまま→renderモジュール以外に原因がある可能性

# correct data

```
loop 0              ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x0606 sp: 0x00fb
loop 1              ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x060d sp: 0x00f9
loop 2              ... a: 0x0002 x: 0x0000 y: 0x0000 pc: 0x060f sp: 0x00f9
loop 3              ... a: 0x0002 x: 0x0000 y: 0x0000 pc: 0x0611 sp: 0x00f9
loop 4              ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x0613 sp: 0x00f9
loop 5              ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x0615 sp: 0x00f9
loop 6              ... a: 0x0011 x: 0x0000 y: 0x0000 pc: 0x0617 sp: 0x00f9
loop 7              ... a: 0x0011 x: 0x0000 y: 0x0000 pc: 0x0619 sp: 0x00f9
loop 8              ... a: 0x0010 x: 0x0000 y: 0x0000 pc: 0x061b sp: 0x00f9
loop 9              ... a: 0x0010 x: 0x0000 y: 0x0000 pc: 0x061d sp: 0x00f9
loop 10             ... a: 0x000f x: 0x0000 y: 0x0000 pc: 0x061f sp: 0x00f9
loop 11             ... a: 0x000f x: 0x0000 y: 0x0000 pc: 0x0621 sp: 0x00f9
loop 12             ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x0623 sp: 0x00f9
loop 13             ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x0625 sp: 0x00f9
loop 14             ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x0627 sp: 0x00f9
loop 15             ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x0629 sp: 0x00f9
loop 16             ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x0609 sp: 0x00fb
loop 17             ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x062a sp: 0x00f9
loop 18             ... a: 0x000c x: 0x0000 y: 0x0000 pc: 0x062c sp: 0x00f9
loop 19             ... a: 0x000c x: 0x0000 y: 0x0000 pc: 0x062e sp: 0x00f9
loop 20             ... a: 0x000d x: 0x0000 y: 0x0000 pc: 0x0630 sp: 0x00f9
loop 21             ... a: 0x0001 x: 0x0000 y: 0x0000 pc: 0x0632 sp: 0x00f9
loop 22             ... a: 0x0001 x: 0x0000 y: 0x0000 pc: 0x0633 sp: 0x00f9
loop 23             ... a: 0x0003 x: 0x0000 y: 0x0000 pc: 0x0635 sp: 0x00f9
loop 24             ... a: 0x0003 x: 0x0000 y: 0x0000 pc: 0x0637 sp: 0x00f9
loop 25             ... a: 0x0003 x: 0x0000 y: 0x0000 pc: 0x060c sp: 0x00fb
loop 26             ... a: 0x0003 x: 0x0000 y: 0x0000 pc: 0x0603 sp: 0x00fd
loop 27             ... a: 0x0003 x: 0x0000 y: 0x0000 pc: 0x0638 sp: 0x00fb
loop 28             ... a: 0x0003 x: 0x0000 y: 0x0000 pc: 0x064d sp: 0x00f9
loop 29             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x064f sp: 0x00f9
loop 30             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x0651 sp: 0x00f9
loop 31             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x0653 sp: 0x00f9
loop 32             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x0655 sp: 0x00f9
loop 33             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x0657 sp: 0x00f9
loop 34             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x0659 sp: 0x00f9
loop 35             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x065b sp: 0x00f9
loop 36             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x065d sp: 0x00f9
loop 37             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x065f sp: 0x00f9
loop 38             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x063b sp: 0x00fb
loop 39             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x068d sp: 0x00f9
loop 40             ... a: 0x0000 x: 0x0000 y: 0x0000 pc: 0x0694 sp: 0x00f7
loop 41             ... a: 0x000c x: 0x0000 y: 0x0000 pc: 0x0696 sp: 0x00f7
loop 42             ... a: 0x000c x: 0x0000 y: 0x0000 pc: 0x0698 sp: 0x00f7
loop 43             ... a: 0x000c x: 0x0000 y: 0x0000 pc: 0x06a7 sp: 0x00f7
loop 44             ... a: 0x000c x: 0x0000 y: 0x0000 pc: 0x0690 sp: 0x00f9
loop 45             ... a: 0x000c x: 0x0000 y: 0x0000 pc: 0x06a8 sp: 0x00f7
loop 46             ... a: 0x000c x: 0x0002 y: 0x0000 pc: 0x06aa sp: 0x00f7
loop 47             ... a: 0x0010 x: 0x0002 y: 0x0000 pc: 0x06ac sp: 0x00f7
loop 48             ... a: 0x0010 x: 0x0002 y: 0x0000 pc: 0x06ae sp: 0x00f7
loop 49             ... a: 0x0010 x: 0x0002 y: 0x0000 pc: 0x06b6 sp: 0x00f7
loop 50             ... a: 0x0010 x: 0x0003 y: 0x0000 pc: 0x06b7 sp: 0x00f7
loop 51             ... a: 0x0010 x: 0x0004 y: 0x0000 pc: 0x06b8 sp: 0x00f7
loop 52             ... a: 0x0010 x: 0x0004 y: 0x0000 pc: 0x06ba sp: 0x00f7
loop 53             ... a: 0x0010 x: 0x0004 y: 0x0000 pc: 0x06c2 sp: 0x00f7
loop 54             ... a: 0x0010 x: 0x0004 y: 0x0000 pc: 0x0693 sp: 0x00f9
loop 55             ... a: 0x0010 x: 0x0004 y: 0x0000 pc: 0x063e sp: 0x00fb
loop 56             ... a: 0x0010 x: 0x0004 y: 0x0000 pc: 0x06c3 sp: 0x00f9
loop 57             ... a: 0x0010 x: 0x0004 y: 0x0000 pc: 0x06c5 sp: 0x00f9
loop 58             ... a: 0x0010 x: 0x0003 y: 0x0000 pc: 0x06c6 sp: 0x00f9
loop 59             ... a: 0x0003 x: 0x0003 y: 0x0000 pc: 0x06c7 sp: 0x00f9
loop 60             ... a: 0x0004 x: 0x0003 y: 0x0000 pc: 0x06c9 sp: 0x00f9
loop 61             ... a: 0x0004 x: 0x0003 y: 0x0000 pc: 0x06cb sp: 0x00f9
loop 62             ... a: 0x0004 x: 0x0002 y: 0x0000 pc: 0x06cc sp: 0x00f9
loop 63             ... a: 0x0004 x: 0x0002 y: 0x0000 pc: 0x06c7 sp: 0x00f9
loop 64             ... a: 0x0010 x: 0x0002 y: 0x0000 pc: 0x06c9 sp: 0x00f9
loop 65             ... a: 0x0010 x: 0x0002 y: 0x0000 pc: 0x06cb sp: 0x00f9
loop 66             ... a: 0x0010 x: 0x0001 y: 0x0000 pc: 0x06cc sp: 0x00f9
loop 67             ... a: 0x0010 x: 0x0001 y: 0x0000 pc: 0x06c7 sp: 0x00f9
loop 68             ... a: 0x0004 x: 0x0001 y: 0x0000 pc: 0x06c9 sp: 0x00f9
loop 69             ... a: 0x0004 x: 0x0001 y: 0x0000 pc: 0x06cb sp: 0x00f9
loop 70             ... a: 0x0004 x: 0x0000 y: 0x0000 pc: 0x06cc sp: 0x00f9
```