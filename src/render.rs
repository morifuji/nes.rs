use crate::{frame::Frame, palette, ppu, ppu::PPU};

fn bg_pallette(ppu: &PPU, tile_column: usize, tile_row: usize) -> [u8; 4] {
    let address = (tile_column / 4) + (tile_row / 4 * 8);
    // ベースaddressが0x2000であるため 0x23C0-0x2000=0x3C0
    let attribute = ppu.vram[0x3C0 + address];

    let palette_no = match ((tile_row % 4) / 2, (tile_column % 4) / 2) {
        (0, 0) => attribute & 0b11,
        (0, 1) => (attribute >> 2) & 0b11,
        (1, 0) => (attribute >> 4) & 0b11,
        (1, 1) => (attribute >> 6) & 0b11,
        _ => panic!(
            "palette can't computed... tile_row, tile_column: {:?}, {:?}",
            tile_row, tile_column
        ),
    };

    [
        ppu.palette_table[0],
        ppu.palette_table[(palette_no * 4 + 1) as usize],
        ppu.palette_table[(palette_no * 4 + 2) as usize],
        ppu.palette_table[(palette_no * 4 + 3) as usize],
    ]
}

pub fn render(ppu: &PPU, frame: &mut Frame) {
    let bank = ppu.ctrl.background_pattern_addr() as usize;
    let bank_offset_address = (bank * 0x1000) as u16;
    //_log::debug!("bank, bank_offset: {}, {}", bank, bank_offset_address);

    for i in 0..(32 * 30) {
        let tile = ppu.vram[i] as u16;
        let tile_x = i % 32;
        let tile_y = i / 32;

        let tile = &ppu.chr_rom[((tile * 16 + bank_offset_address) as usize)
            ..=((tile * 16 + 15 + bank_offset_address) as usize)];

        let palette = bg_pallette(ppu, tile_x, tile_y);

        // 8x8のタイル
        for y in 0..8 {
            // leftとrightで保存場所が別れる
            let mut upper = tile[y];
            let mut lower = tile[y + 8];

            for x in (0..8).rev() {
                // ex) upper: 0b01010101;
                // ex) lower: 0b00001111;
                // →   value: 0b00000011;
                let color = (1 & lower) << 1 | (1 & upper);
                upper = upper >> 1;
                lower = lower >> 1;

                let rgb = match color {
                    0 => palette::SYSTEM_PALLETE[palette[0] as usize],
                    1 => palette::SYSTEM_PALLETE[palette[1] as usize],
                    2 => palette::SYSTEM_PALLETE[palette[2] as usize],
                    3 => palette::SYSTEM_PALLETE[palette[3] as usize],
                    _ => panic!("can't display"),
                };

                frame.set_pixel(tile_x * 8 + x, tile_y * 8 + y, rgb);
            }
        }
    }

    
    for i in (0..ppu.oam_data.len()).step_by(4).rev() {
        let tile_y = ppu.oam_data[i] as usize;
        let index = ppu.oam_data[i + 1];
        let attribute = ppu.oam_data[i + 2];
        let tile_x = ppu.oam_data[i + 3] as usize;

        // Priority (0: in front of background; 1: behind background)
        if attribute >> 5 & 1 == 1 {
            continue
        }

        let is_8x16 = ppu
            .ctrl
            .contains(ppu::register::control::ControlRegister::SPRITE_SIZE);
        let (tile_index, bank_offset) = if is_8x16 {
            (
                index >> 7, 
                if (index & 1) == 1 { 0x1000 as u16 } else { 0x0000 })
        } else {
            (
                index,
                if ppu
                    .ctrl
                    .contains(ppu::register::control::ControlRegister::SPRITE_PATTERN_ADDR)
                {
                    0x1000 as u16
                } else {
                    0x0000
                },
            )
        };

        // NOTE: 参照実装
        let tile_index = ppu.oam_data[i + 1] as u16;
        let bank_offset = if !ppu.ctrl.contains(ppu::register::control::ControlRegister::SPRITE_PATTERN_ADDR) {
            0
        } else {
            0x1000
        };

        let tile = &ppu.chr_rom[((tile_index as u16 * 16 + bank_offset) as usize)
            ..=((tile_index as u16 * 16 + 15 + bank_offset) as usize)];

        let flip_vertical = attribute >> 7 & 1 == 1;
        let flip_horizontal = attribute >> 6 & 1 == 1;

        let palette_index = ppu.oam_data[i + 2] & 0b11;
        // let palette = ppu.palette_table[(palette_index+4) as usize];
        let palette = sprite_palette(&ppu, palette_index as usize);


        //_log::debug!("[{}] {:?}, {:?}, {:#06b}, {:?}, {:?}", i/4,palette, &ppu.oam_data[i..i+4], attribute, tile, bank_offset);

        for y in 0..=7 {
            let mut upper = tile[y];
            let mut lower = tile[y + 8];
            'row_loop: for x in (0..=7).rev() {
                let value = (1 & lower) << 1 | (1 & upper);
                upper = upper >> 1;
                lower = lower >> 1;
                let rgb = match value {
                    0 => continue 'row_loop,
                    1 => palette::SYSTEM_PALLETE[palette[1] as usize],
                    2 => palette::SYSTEM_PALLETE[palette[2] as usize],
                    3 => palette::SYSTEM_PALLETE[palette[3] as usize],
                    _ => panic!("can't be"),
                };
                match (flip_horizontal, flip_vertical) {
                    (false, false) => frame.set_pixel(tile_x + x, tile_y + y, rgb),
                    (true, false) => frame.set_pixel(tile_x + 7 - x, tile_y + y, rgb),
                    (false, true) => frame.set_pixel(tile_x + x, tile_y + 7 - y, rgb),
                    (true, true) => frame.set_pixel(tile_x + 7 - x, tile_y + 7 - y, rgb),
                }
            }
        }
    }
}

fn sprite_palette(ppu: &PPU, palette_no: usize) -> [u8; 4] {
    [
        0, // trasparent
        ppu.palette_table[16 + (palette_no * 4 + 1) as usize],
        ppu.palette_table[16 + (palette_no * 4 + 2) as usize],
        ppu.palette_table[16 + (palette_no * 4 + 3) as usize],
    ]
}


// [0, 39, 32, 6], [0, 0, 0, 0], [0, 3, 15, 31, 63, 63, 127, 127, 0, 0, 0, 0, 0, 0, 0, 0], 4096
// [0, 39, 32, 6], [0, 0, 0, 0], [0, 3, 15, 31, 63, 63, 127, 127, 0, 0, 0, 0, 0, 0, 0, 0], 4096

// [0, 39, 32, 6], [171, 1, 0, 155], [0, 192, 240, 248, 248, 252, 252, 252, 0, 0, 0, 0, 0, 0, 0, 0], 4096
// [0, 39, 32, 6], [0, 0, 0, 0], [0, 3, 15, 31, 63, 63, 127, 127, 0, 0, 0, 0, 0, 0, 0, 0], 4096
// [0, 39, 32, 6], [171, 0, 64, 154], [0, 3, 15, 31, 63, 63, 127, 127, 0, 0, 0, 0, 0, 0, 0, 0], 4096
