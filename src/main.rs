#![feature(bigint_helper_methods)]

#[macro_use]
extern crate lazy_static;

pub mod bus;
pub mod cartridge;
pub mod frame;
pub mod joypad;
pub mod opcodes;
pub mod palette;
pub mod ppu;
pub mod render;
pub mod trace;

use bus::Bus;
use cartridge::Rom;
use frame::Frame;
use ppu::PPU;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::pixels::PixelFormatEnum;
use sdl2::EventPump;
use std::collections::HashMap;
use std::fs;

use log::{debug, error, info, log_enabled, Level};

const PROGRAM_OFFSET: u16 = 0x0600;

const WIDTH_PX: u16 = 256;
const HEIGHT_PX: u16 = 240;

const SCALE: u16 = 2;

fn main() {
    env_logger::init();

    let mut key_map = HashMap::new();
    key_map.insert(Keycode::Down, joypad::JoypadButton::DOWN);
    key_map.insert(Keycode::Up, joypad::JoypadButton::UP);
    key_map.insert(Keycode::Right, joypad::JoypadButton::RIGHT);
    key_map.insert(Keycode::Left, joypad::JoypadButton::LEFT);
    key_map.insert(Keycode::Space, joypad::JoypadButton::SELECT);
    key_map.insert(Keycode::Return, joypad::JoypadButton::START);
    key_map.insert(Keycode::A, joypad::JoypadButton::BUTTON_A);
    key_map.insert(Keycode::S, joypad::JoypadButton::BUTTON_B);

    // initialize for SDL
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let window = video_subsystem
        .window(
            "Snake game",
            (WIDTH_PX * SCALE) as u32,
            (HEIGHT_PX * SCALE) as u32,
        )
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().present_vsync().build().unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();
    canvas.set_scale(SCALE as f32, SCALE as f32).unwrap();

    // prepare texture
    let creator = canvas.texture_creator();
    let mut texture = creator
        .create_texture_target(PixelFormatEnum::RGB24, WIDTH_PX as u32, HEIGHT_PX as u32)
        .unwrap();

    //load the game
    // let bytes = std::fs::read("nestest.nes").unwrap();
    // let rom = Rom::new(&bytes).unwrap();

    // let tile_no_include_bank_no = 0;

    // let (position_x, position_y) = no_2_position(&tile_no_include_bank_no);

    // let screen_rect = Rect::new(position_x as i32, position_y as i32, 8,8);

    // let tile_frame = show_tile(&rom.chr_rom, 1 as usize, 0 as usize);

    // // texture.update(screen_rect, &tile_frame.data, 256*3).unwrap();
    // texture.update(screen_rect, &tile_frame.data, 256*3).unwrap();

    // // canvas.copy(&texture, None, screen_rect).unwrap();
    // canvas.copy(&texture, None, None).unwrap();

    // canvas.present();

    // for bank_no in (0..=1).rev() {
    //     for i in 0..256 {
    //         let tile_no_include_bank_no = i+(bank_no * 256);

    //         let (position_x, position_y) = no_2_position(&tile_no_include_bank_no);

    //         let screen_rect = Rect::new(position_x as i32, position_y as i32, 8,8);

    //         let tile_frame = show_tile(&rom.chr_rom, bank_no as usize, i as usize);

    //         // texture.update(screen_rect, &tile_frame.data, 256*3).unwrap();
    //         texture.update(screen_rect, &tile_frame.data, 256*3).unwrap();

    //         // canvas.copy(&texture, None, screen_rect).unwrap();
    //         canvas.copy(&texture, None, None).unwrap();

    //         canvas.present();
    //     }
    // }

    // loop {
    //     for event in event_pump.poll_iter() {
    //        match event {
    //          Event::Quit { .. }
    //          | Event::KeyDown {
    //              keycode: Some(Keycode::Escape),
    //              ..
    //          } => std::process::exit(0),
    //          _ => { /* do nothing */ }
    //        }
    //     }
    //  }

    let rom_file = fs::read("./nestest.nes").expect("can't load rom file...");
    let rom = Rom::new(&rom_file).expect("can't convert Rom...");

    let mut frame = frame::Frame::new();

    let bus = Bus::new(rom, move |ppu: &PPU, joypad: &mut joypad::Joypad| {
        render::render(ppu, &mut frame);

        texture.update(None, &frame.data, 256 * 3);

        canvas.copy(&texture, None, None).unwrap();

        canvas.present();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => std::process::exit(0),

                Event::KeyDown { keycode, .. } => {
                    if let Some(key) = key_map.get(&keycode.unwrap_or(Keycode::Ampersand)) {
                        joypad.set_button_pressed_status(*key, true);
                    }
                }
                Event::KeyUp { keycode, .. } => {
                    if let Some(key) = key_map.get(&keycode.unwrap_or(Keycode::Ampersand)) {
                        joypad.set_button_pressed_status(*key, false);
                    }
                }

                _ => { /* do nothing */ }
            }
        }
    });

    let mut cpu = CPU::new(bus);
    cpu.reset();

    // cpu.run();
    cpu.run_with_callback(move |cpu| {
        log::debug!("{}", trace::trace(cpu));
        //_log::debug!("{},{}", cpu.memory_read(0x2002),trace::trace(cpu));

        // handle_user_input(cpu, &mut event_pump);

        // cpu.memory_write(0xfe, rng.gen_range(1, 16));
        // if read_screen_state(cpu, &mut screen_state) {
        //     texture.update(None, &screen_state, 32 * 3).unwrap();
        //     canvas.copy(&texture, None, None).unwrap();
        //     canvas.present();
        // }

        // ::std::thread::sleep(std::time::Duration::new(0, 70_000));
        // i += 1;
    });
}

// bankは、tileのグループ、各bankでtile_nが存在する
pub fn show_tile(chr_rom: &Vec<u8>, bank_no: usize, tile_n: usize) -> Frame {
    assert!(bank_no <= 1);
    let mut frame = frame::Frame::new();
    // bankのoffsetを計算
    let bank_offset_address = (bank_no * 0x1000) as usize;

    // 1つのタイル範囲
    let tile =
        &chr_rom[(bank_offset_address + tile_n * 16)..=(bank_offset_address + tile_n * 16 + 15)];

    // 8x8のタイル
    for y in 0..7 {
        // leftとrightで保存場所が別れる
        let mut upper = tile[y];
        let mut lower = tile[y + 8];

        for x in (0..7).rev() {
            // ex) upper: 0b01010101;
            // ex) lower: 0b00001111;
            // →   value: 0b00000011;
            let color = (1 & upper) << 1 | (1 & lower);
            upper = upper >> 1;
            lower = lower >> 1;

            let rgb = match color {
                0 => palette::SYSTEM_PALLETE[0x01],
                1 => palette::SYSTEM_PALLETE[0x23],
                2 => palette::SYSTEM_PALLETE[0x27],
                3 => palette::SYSTEM_PALLETE[0x30],
                _ => panic!("can't display"),
            };

            frame.set_pixel(x, y, rgb);
        }
    }

    frame
}

fn handle_user_input(cpu: &mut CPU, event_pump: &mut EventPump) {
    for event in event_pump.poll_iter() {
        match event {
            Event::Quit { .. }
            | Event::KeyDown {
                keycode: Some(Keycode::Escape),
                ..
            } => std::process::exit(0),
            Event::KeyDown {
                keycode: Some(Keycode::W),
                ..
            } => {
                cpu.memory_write(0xff, 0x77);
            }
            Event::KeyDown {
                keycode: Some(Keycode::S),
                ..
            } => {
                cpu.memory_write(0xff, 0x73);
            }
            Event::KeyDown {
                keycode: Some(Keycode::A),
                ..
            } => {
                cpu.memory_write(0xff, 0x61);
            }
            Event::KeyDown {
                keycode: Some(Keycode::D),
                ..
            } => {
                cpu.memory_write(0xff, 0x64);
            }
            _ => { /* do nothing */ }
        }
    }
}
fn color(byte: u8) -> Color {
    match byte {
        0 => sdl2::pixels::Color::BLACK,
        1 => sdl2::pixels::Color::WHITE,
        2 | 9 => sdl2::pixels::Color::GREY,
        3 | 10 => sdl2::pixels::Color::RED,
        4 | 11 => sdl2::pixels::Color::GREEN,
        5 | 12 => sdl2::pixels::Color::BLUE,
        6 | 13 => sdl2::pixels::Color::MAGENTA,
        7 | 14 => sdl2::pixels::Color::YELLOW,
        _ => sdl2::pixels::Color::CYAN,
    }
}

fn read_screen_state(cpu: &mut CPU, frame: &mut [u8; 32 * 3 * 32]) -> bool {
    let mut frame_idx = 0;
    let mut update = false;
    for i in 0x0200..0x600 {
        let color_idx = cpu.memory_read(i as u16);
        let (b1, b2, b3) = color(color_idx).rgb();
        if frame[frame_idx] != b1 || frame[frame_idx + 1] != b2 || frame[frame_idx + 2] != b3 {
            frame[frame_idx] = b1;
            frame[frame_idx + 1] = b2;
            frame[frame_idx + 2] = b3;
            update = true;
        }
        frame_idx += 3;
    }
    update
}

const STACK: u16 = 0x0100;

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub enum AddressingMode {
    Immediate,
    ZeroPage,
    ZeroPage_X,
    ZeroPage_Y,
    Absolute,
    Absolute_X,
    Absolute_Y,
    Indirect_X,
    Indirect_Y,
    NoneAddressing,
}

pub struct CPU<'a> {
    pub register_a: u8,
    // ステータスレジスタP
    pub status: u8,
    // プログラムの位置を表す
    pub program_counter: u16,
    // インデックスレジスタ
    pub register_x: u8,
    // インデックスレジスタ
    pub register_y: u8,

    // Bus
    pub bus: bus::Bus<'a>,

    // スタックポインタ
    // 8bit
    pub stack_pointer: u8,
}

impl<'a> CPU<'a> {
    pub fn run(&mut self) {
        self.run_with_callback(|_| {});
    }

    pub fn new<'b>(bus: Bus<'b>) -> CPU<'b> {
        CPU {
            register_a: 0,
            status: 0,
            program_counter: 0,
            register_x: 0,
            register_y: 0,
            bus,
            // 初期値は0xFD、
            // NOTE: https://taotao54321.hatenablog.com/entry/2017/04/09/151355
            stack_pointer: 0xfd,
        }
    }

    /**
     * https://www.masswerk.at/6502/6502_instruction_set.html#LDA
     */
    fn lda(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);
        //_log::debug!("value is {:#04x}", value);

        self.register_a = value;
        self.update_zero_and_negative_flags(self.register_a);

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    fn ldx(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        self.register_x = value;
        self.update_zero_and_negative_flags(self.register_x);

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    fn ldy(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        self.register_y = value;
        self.update_zero_and_negative_flags(self.register_y);

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    /**
     * https://www.masswerk.at/6502/6502_instruction_set.html#TAX
     */
    fn tax(&mut self) {
        self.register_x = self.register_a;
        self.update_zero_and_negative_flags(self.register_x);
    }
    fn tay(&mut self) {
        self.register_y = self.register_a;
        self.update_zero_and_negative_flags(self.register_y);
    }
    fn tsx(&mut self) {
        self.register_x = self.stack_pointer;
        self.update_zero_and_negative_flags(self.register_x);
    }

    fn txa(&mut self) {
        self.register_a = self.register_x;
        self.update_zero_and_negative_flags(self.register_x);
    }
    fn txs(&mut self) {
        self.stack_pointer = self.register_x;
        // なぜ不要か不明？？
        // self.update_zero_and_negative_flags(self.stack_pointer);
    }

    fn tya(&mut self) {
        self.register_a = self.register_y;
        self.update_zero_and_negative_flags(self.register_a);
    }

    fn pha(&mut self) {
        let value = self.register_a;
        self.stack_push(value);
    }
    fn pla(&mut self) {
        let value = self.stack_pop();
        self.register_a = value;
        self.update_zero_and_negative_flags(self.register_a);
    }
    fn php(&mut self) {
        let mut value = self.status;
        // nestest.romで出てきた処理
        if value & 0b0001_0000 == 0 {
            value = value | 0b0001_0000;
        }
        if value & 0b0010_0000 == 0 {
            value = value | 0b0010_0000;
        }
        // flags.insert(CpuFlags::BREAK);
        // flags.insert(CpuFlags::BREAK2);
        self.stack_push(value);
    }
    fn plp(&mut self) {
        let mut value = self.stack_pop();
        if value & 0b0001_0000 != 0 {
            value = value & !0b0001_0000;
        }
        if value & 0b0010_0000 == 0 {
            value = value | 0b0010_0000;
        }
        self.status = value;
    }

    fn add_to_register_a(&mut self, data: u8) {
        // A - メモリ - キャリーフラグの反転
        // 40 -

        // 40+255+
        let has_carry_flag = (self.status & 1) != 0;

        // NOTE: https://doc.rust-lang.org/std/primitive.u64.html#method.carrying_add
        let (result, has_carried) = self.register_a.carrying_add(data, has_carry_flag);

        self.update_zero_and_negative_flags(result);

        // NOTE: キャリーとオーバーフロー https://zenn.dev/szktty/articles/nes-carry-overflow
        // CARRYフラグ更新
        if has_carried {
            self.status = self.status | 1;
        } else {
            self.status = self.status & !(1 as u8);
        }

        // オーバーフロー
        if (data ^ result) & (result ^ self.register_a) & 0x80 != 0 {
            self.status = self.status | 0b0100_0000;
        } else {
            self.status = self.status & !0b0100_0000;
        }
        self.register_a = result;
    }

    fn adc(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);
        self.add_to_register_a(value);

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    fn sbc(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        self.add_to_register_a(value.wrapping_neg().wrapping_sub(1));

        // let (result, has_overflowed) = self.register_a.overflowing_add(value.wrapping_neg().wrapping_sub(1));
        // self.register_a = result;

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    /**
     * https://www.masswerk.at/6502/6502_instruction_set.html#AND
     */
    fn and(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        self.register_a = self.register_a & value;
        self.update_zero_and_negative_flags(self.register_a);

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    /**
     * https://www.masswerk.at/6502/6502_instruction_set.html#EOR
     */
    fn eor(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        self.register_a = self.register_a ^ value;
        self.update_zero_and_negative_flags(self.register_a);

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    /**
     * https://www.masswerk.at/6502/6502_instruction_set.html#ORA
     */
    fn ora(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        self.register_a = self.register_a | value;
        self.update_zero_and_negative_flags(self.register_a);

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    fn asl_with_memory(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        // シフト前に計算する必要あり
        self.update_carry_flag(value, 7);

        let calc_value = value << 1;
        self.memory_write(addr, calc_value);
        self.update_zero_and_negative_flags(calc_value);
    }
    fn asl_with_a(&mut self) {
        // シフト前に計算する必要あり
        self.update_carry_flag(self.register_a, 7);

        self.register_a = self.register_a << 1;
        self.update_zero_and_negative_flags(self.register_a);
    }

    fn lsr_with_memory(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        // シフト前に計算する必要あり
        self.update_carry_flag(value, 0);

        let calc_value = value >> 1;
        self.memory_write(addr, calc_value);
        self.update_zero_and_negative_flags(calc_value);
    }
    fn lsr_with_a(&mut self) {
        // シフト前に計算する必要あり
        self.update_carry_flag(self.register_a, 0);

        self.register_a = self.register_a >> 1;
        self.update_zero_and_negative_flags(self.register_a);
    }

    fn rol_with_memory(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        // シフト前に計算する必要あり
        self.update_carry_flag(value, 7);

        let calc_value = value.rotate_left(1);
        self.memory_write(addr, calc_value);
        self.update_zero_and_negative_flags(calc_value);
    }
    fn rol_with_a(&mut self) {
        // シフト前に計算する必要あり
        self.update_carry_flag(self.register_a, 7);

        self.register_a = self.register_a.rotate_left(1);
        self.update_zero_and_negative_flags(self.register_a);
    }

    fn ror_with_memory(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        let old_status = self.status;

        // and the original bit 0 is shifted into the Carry.
        if value & 0b1 != 0 {
            self.status = self.status | 0b1;
        } else {
            self.status = self.status & !0b1;
        }

        let mut calc_value = value.rotate_right(1);
        // キャリーがある場合は7ビット目をたてる
        // The Carry is shifted into bit 7 and
        if old_status & 0b1 != 0 {
            calc_value = calc_value | 0b1000_0000;
        } else {
            calc_value = calc_value & !0b1000_0000;
        }

        self.memory_write(addr, calc_value);
        self.update_zero_and_negative_flags(calc_value);
    }

    fn ror_with_a(&mut self) {
        // シフト前に計算する必要あり
        // self.update_carry_flag(self.register_a, 0);
        let old_status = self.status;

        // and the original bit 0 is shifted into the Carry.
        if self.register_a & 0b1 != 0 {
            self.status = self.status | 0b1;
        } else {
            self.status = self.status & !0b1;
        }

        self.register_a = self.register_a.rotate_right(1);

        // キャリーがある場合は7ビット目をたてる
        // The Carry is shifted into bit 7 and
        if old_status & 0b1 != 0 {
            self.register_a = self.register_a | 0b1000_0000;
        } else {
            self.register_a = self.register_a & !0b1000_0000;
        }

        // self.register_a = self.register_a >> 1;

        self.update_zero_and_negative_flags(self.register_a);
    }

    fn inc(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        let calc_value = value.wrapping_add(1);
        self.memory_write(addr, calc_value);
        self.update_zero_and_negative_flags(calc_value);
    }

    /**
     * https://www.masswerk.at/6502/6502_instruction_set.html#INX
     */
    fn inx(&mut self) {
        (self.register_x, _) = self.register_x.overflowing_add(0x0000_0001);
        self.update_zero_and_negative_flags(self.register_x);
    }

    /**
     * https://www.masswerk.at/6502/6502_instruction_set.html#INY
     */
    fn iny(&mut self) {
        (self.register_y, _) = self.register_y.overflowing_add(0x0000_0001);
        self.update_zero_and_negative_flags(self.register_y);
    }

    fn dec(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        let value = self.memory_read(addr);

        let calc_value = value.wrapping_sub(1);
        self.memory_write(addr, calc_value);
        self.update_zero_and_negative_flags(calc_value);
    }

    fn dex(&mut self) {
        (self.register_x, _) = self.register_x.overflowing_sub(0x0000_0001);
        self.update_zero_and_negative_flags(self.register_x);
    }

    fn dey(&mut self) {
        (self.register_y, _) = self.register_y.overflowing_sub(1);
        self.update_zero_and_negative_flags(self.register_y);
    }

    /**
     * http://6502.org/tutorials/compare_instructions.html
     */
    fn cmp(&mut self, mode: &AddressingMode) {
        self.compare(mode, self.register_a);
    }

    fn cpx(&mut self, mode: &AddressingMode) {
        self.compare(mode, self.register_x);
    }

    fn cpy(&mut self, mode: &AddressingMode) {
        self.compare(mode, self.register_y);
    }

    fn compare(&mut self, mode: &AddressingMode, target: u8) {
        let (addr, is_cross_page) = self.get_operand_address(mode);
        let value = self.memory_read(addr);
        //carryフラグ
        // WARNING: これは正確には、オペランドからA・X・Yを引くイメージ。→言い方を変えると、A・X・Yからオペランドを引いてみて結果は捨ててフラグだけ立てる。フラグの立ち方はVが変化しない以外はSBCと同じ。
        // equalはcarryフラグをON
        if target >= value {
            // セット
            self.status = self.status | 0b0000_0001;
        } else {
            // クリア
            self.status = self.status & !0b0000_0001;
        }

        self.update_zero_and_negative_flags(target.wrapping_sub(value));

        if is_cross_page {
            self.bus.tick(1);
        }
    }

    fn jmp_absolute(&mut self) {
        let pc = self.memory_read_u16(self.program_counter);
        self.program_counter = pc;
    }

    fn jmp_indirect(&mut self) {
        let pc_address = self.memory_read_u16(self.program_counter);
        self.program_counter = self.memory_read_u16(pc_address);

        // REF: https://github.com/bugzmanov/nes_ebook/blob/master/code/ch3.3/src/cpu.rs#L684
        // 6502のバグらしい。
        let indirect_ref = if pc_address & 0x00FF == 0x00FF {
            let lo = self.memory_read(pc_address);
            let hi = self.memory_read(pc_address & 0xFF00);
            (hi as u16) << 8 | (lo as u16)
        } else {
            self.memory_read_u16(pc_address)
        };

        self.program_counter = indirect_ref;
    }

    fn jsr(&mut self) {
        // スタックポインタを減らす
        // WHY?→NOTE: https://pgate1.at-ninja.jp/NES_on_FPGA/nes_cpu.htm
        // > ジャンプサブルーチン命令（JSR）によってスタックに格納する復帰アドレスは、 次の命令の一つ前のアドレス（JSRの最後のバイト）であり、 リターンサブルーチン命令（RTS）によってインクリメントします。
        self.stack_push_u16(self.program_counter.wrapping_add(2 - 1) as u16);
        self.jmp_absolute();
    }

    fn rts(&mut self) {
        //
        let return_address = self.stack_pop_u16() + 1;
        self.program_counter = return_address;
    }

    fn rti(&mut self) {
        self.status = self.stack_pop();

        let status_break = 0b0001_0000;
        // breakフラグが1の場合
        if self.status & status_break != 0 {
            self.status = self.status & !status_break;
        }
        // 予約済み	ステータス は必須
        if self.status & 0b0010_0000 == 0 {
            self.status = self.status | 0b0010_0000;
        }

        self.program_counter = self.stack_pop_u16();
    }

    fn bne(&mut self) {
        if self.status & 0b0000_0010 == 0 {
            self.branch();
        } else {
            self.program_counter += 1;
        }
    }

    fn bvs(&mut self) {
        if self.status & 0b0100_0000 != 0 {
            self.branch();
        } else {
            self.program_counter += 1;
        }
    }
    fn bvc(&mut self) {
        if self.status & 0b0100_0000 == 0 {
            self.branch();
        } else {
            self.program_counter += 1;
        }
    }
    fn bmi(&mut self) {
        if self.status & 0b1000_0000 != 0 {
            self.branch();
        } else {
            self.program_counter += 1;
        }
    }
    fn beq(&mut self) {
        if self.status & 0b0000_0010 != 0 {
            self.branch();
        } else {
            self.program_counter += 1;
        }
    }
    fn bcs(&mut self) {
        if self.status & 0b0000_0001 != 0 {
            self.branch();
        } else {
            self.program_counter += 1;
        }
    }
    fn bcc(&mut self) {
        if self.status & 0b0000_0001 == 0 {
            self.branch();
        } else {
            self.program_counter += 1;
        }
    }
    fn bpl(&mut self) {
        if self.status & 0b1000_0000 == 0 {
            self.branch();
        } else {
            self.program_counter += 1;
        }
    }

    fn bit(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        let value = self.memory_read(addr);
        let result = self.register_a & value;

        // NOTE: https://wentwayup.tamaliver.jp/e166485.html
        // Nフラグがオペランドの7ビット目、Vフラグがオペランドの6ビット目の値になり、オペランドとアキュムレーターのANDの結果がゼロならZフラグが立つ。
        if result == 0 {
            self.status = self.status | 0b0000_0010;
        } else {
            self.status = self.status & !0b0000_0010;
        }

        // 6/V
        if value & 0b0100_0000 != 0 {
            self.status = self.status | 0b0100_0000;
        } else {
            self.status = self.status & !0b0100_0000;
        }

        // 7/N
        if value & 0b1000_0000 != 0 {
            self.status = self.status | 0b1000_0000;
        } else {
            self.status = self.status & !0b1000_0000;
        }
    }

    fn sta(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        self.memory_write(addr, self.register_a);
    }

    fn stx(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        self.memory_write(addr, self.register_x);
    }

    fn sty(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(mode);
        self.memory_write(addr, self.register_y);
    }

    fn cli(&mut self) {
        if self.status & 0b0000_0100 != 0 {
            self.status = self.status & !0b0000_0100;
        }
    }
    fn sei(&mut self) {
        if self.status & 0b0000_0100 == 0 {
            self.status = self.status | 0b0000_0100;
        }
    }

    fn clv(&mut self) {
        if self.status & 0b0100_0000 != 0 {
            self.status = self.status & !0b0100_0000;
        }
    }

    fn clc(&mut self) {
        if self.status & 0b0000_0001 != 0 {
            self.status = self.status & !0b0000_0001;
        }
    }
    fn sec(&mut self) {
        if self.status & 0b0000_0001 == 0 {
            self.status = self.status | 0b0000_0001;
        }
    }

    fn cld(&mut self) {
        if self.status & 0b0000_1000 != 0 {
            self.status = self.status & !0b0000_1000;
        }
    }

    fn sed(&mut self) {
        if self.status & 0b0000_1000 == 0 {
            self.status = self.status | 0b0000_1000;
        }
    }

    fn lax(&mut self, mode: &AddressingMode) {
        let (addr, is_cross_page) = self.get_operand_address(&mode);
        let data = self.memory_read(addr);
        // self.set_register_a(data);

        self.register_a = data;
        self.update_zero_and_negative_flags(self.register_a);

        if is_cross_page {
            self.bus.tick(1);
        }
        self.register_x = self.register_a;
    }

    fn dcp(&mut self, mode: &AddressingMode) {
        let (addr, _) = self.get_operand_address(&mode);
        let mut data = self.memory_read(addr);
        data = data.wrapping_sub(1);
        self.memory_write(addr, data);
        if data <= self.register_a {
            // carry をon
            if self.status & 1 == 0 {
                self.status = self.status | 1;
            }
        }

        self.update_zero_and_negative_flags(self.register_a.wrapping_sub(data));
    }

    fn branch(&mut self) {
        self.bus.tick(1);

        // NOTE: i8への変換必須、-128~127にするため
        // NOTE: N, V, Z, C フラグに基づく分岐ができます。分岐先オフセットは符号付き 8bit なので、範囲は [-128, 127] に制限されます。この範囲外へ飛びたい場合は JMP 命令などを併用する必要があります。
        // NOTE: https://stackoverflow.com/questions/53453628/how-do-i-add-a-signed-integer-to-an-unsigned-integer-in-rust
        let target = self.memory_read(self.program_counter) as i8;
        // WHY add 1?
        let target_addres = self
            .program_counter
            .wrapping_add(1)
            .wrapping_add(target as u16);

        if self.program_counter.wrapping_add(1) & 0xFF00 != target_addres & 0xFF00 {
            self.bus.tick(1);
        }

        self.program_counter = target_addres;
    }

    fn stack_push_u16(&mut self, data: u16) {
        let hi = (data >> 8) as u8;
        let lo = (data & 0xff) as u8;
        self.stack_push(hi);
        self.stack_push(lo);
        // self.memory_write(STACK+(self.stack_pointer as u16), data);
        // self.stack_pointer += 1;
    }

    fn stack_push(&mut self, data: u8) {
        self.memory_write(STACK + (self.stack_pointer as u16), data);
        self.stack_pointer -= 1;
    }

    fn stack_pop_u16(&mut self) -> u16 {
        let lo = self.stack_pop() as u16;
        let hi = self.stack_pop() as u16;
        (hi << 8) | lo
    }

    fn stack_pop(&mut self) -> u8 {
        self.stack_pointer += 1;
        self.memory_read(STACK + (self.stack_pointer as u16))
    }

    fn update_carry_flag(&mut self, value: u8, bit: u8) {
        if (value >> bit) & 1 != 0 {
            self.status = self.status | 1;
        } else {
            self.status = self.status & !1;
        }
    }

    fn update_zero_and_negative_flags(&mut self, register: u8) {
        // NOTE: http://hp.vector.co.jp/authors/VA042397/nes/6502.html
        // 1→演算結果が0になった場合セットされます。ロード命令でも変化します。
        // 7→Aの7ビット目と同じになります。負数の判定用。
        if register == 0 {
            self.status = self.status | 0b0000_0010;
        } else {
            self.status = self.status & !0b0000_0010;
        }

        if register & 0b1000_0000 != 0 {
            self.status = self.status | 0b1000_0000;
        } else {
            self.status = self.status & !0b1000_0000;
        }
        // if register == 0 {
        //     self.status = self.status | 0b0000_0010;
        // } else {
        //     self.status = self.status & !0b0000_0010;
        // }

        // if register >> 7 == 1 {
        //     self.status = self.status | 0b1000_0000;
        // } else {
        //     self.status = self.status & !0b1000_0000;
        // }
    }

    fn memory_read(&mut self, addr: u16) -> u8 {
        self.bus.mem_read(addr)
    }

    fn memory_write(&mut self, addr: u16, data: u8) {
        self.bus.mem_write(addr, data);
    }

    fn memory_read_u16(&mut self, addr: u16) -> u16 {
        let lo = self.memory_read(addr) as u16;
        let hi = self.memory_read(addr + 1) as u16;
        (hi << 8) | lo
    }

    fn memory_write_u16(&mut self, addr: u16, data: u16) {
        let hi = (data >> 8) as u8;
        let lo = (data & 0xff) as u8;
        self.memory_write(addr, lo);
        self.memory_write(addr + 1, hi);
    }

    fn reset(&mut self) {
        self.register_a = 0;
        self.register_x = 0;
        self.register_y = 0;
        self.stack_pointer = 0xFD;
        self.status = 0b100100;

        // self.program_counter = self.memory_read_u16(0xFFFC);

        // FOR nestest
        self.program_counter = 0xC000;
    }

    pub fn load_and_run(&mut self, program: Vec<u8>) {
        self.load(program);
        self.reset();
        self.run();
    }

    fn load(&mut self, program: Vec<u8>) {
        for i in 0..(program.len() as u16) {
            self.bus.mem_write(0x0600 + i, program[i as usize]);
        }
        // FIXME: RAMの範囲外にアクセスするとBUSが動作しないため、一時的にpcに直接代入
        // self.memory_write_u16(0xFFFC, 0x0600);
        self.program_counter = PROGRAM_OFFSET;
    }

    fn interrupt_nmi(&mut self) {
        self.stack_push_u16(self.program_counter);
        let mut status = self.status.clone();
        // BRK
        if status & 0b0001_0000 != 0 {
            status = status & !0b0001_0000;
        }
        // BRK2
        if status & 0b0010_0000 == 0 {
            status = status | 0b0010_0000;
        }

        self.stack_push(status);

        // IRQフラグたたせる
        if self.status & 0b0100 == 0 {
            self.status = self.status & 0b0000_0100;
        }

        // WHY
        self.bus.tick(2);
        self.program_counter = self.memory_read_u16(0xFFFA);
    }

    pub fn run_with_callback<F>(&mut self, mut callback: F)
    where
        F: FnMut(&mut CPU),
    {
        let ref opcodes: HashMap<u8, &'static opcodes::OpCode> = *opcodes::OPCODES_MAP;

        loop {
            if let Some(_nmi) = self.bus.poll_nmi_status() {
                self.interrupt_nmi();
            }

            callback(self);

            let opscode = self.memory_read(self.program_counter);

            self.program_counter += 1;
            let program_counter_state = self.program_counter;

            // FIXME: hashmap使ってスマートにする
            match opscode {
                // NOP
                0xEA => {
                    // 不要
                    // self.program_counter += 1;
                }

                // ADC
                0x69 => {
                    self.adc(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0x65 => {
                    self.adc(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x75 => {
                    self.adc(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x6d => {
                    self.adc(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x7d => {
                    self.adc(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0x79 => {
                    self.adc(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0x61 => {
                    self.adc(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0x71 => {
                    self.adc(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }

                // SBC
                0xE9 => {
                    self.sbc(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0xE5 => {
                    self.sbc(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xF5 => {
                    self.sbc(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0xED => {
                    self.sbc(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xFD => {
                    self.sbc(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0xF9 => {
                    self.sbc(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0xE1 => {
                    self.sbc(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0xF1 => {
                    self.sbc(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }

                0xEB => {
                    let (addr, _) = self.get_operand_address(&AddressingMode::Immediate);
                    let data = self.memory_read(addr);
                    self.add_to_register_a(((data as i8).wrapping_neg().wrapping_sub(1)) as u8);
                    self.program_counter += 1;
                }

                // AND
                0x29 => {
                    self.and(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0x25 => {
                    self.and(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x35 => {
                    self.and(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x2D => {
                    self.and(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x3D => {
                    self.and(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0x39 => {
                    self.and(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0x21 => {
                    self.and(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0x31 => {
                    self.and(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }

                // EOR
                0x49 => {
                    self.eor(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0x45 => {
                    self.eor(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x55 => {
                    self.eor(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x4D => {
                    self.eor(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x5D => {
                    self.eor(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0x59 => {
                    self.eor(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0x41 => {
                    self.eor(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0x51 => {
                    self.eor(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }

                // ORA
                0x09 => {
                    self.ora(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0x05 => {
                    self.ora(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x15 => {
                    self.ora(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x0D => {
                    self.ora(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x1D => {
                    self.ora(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0x19 => {
                    self.ora(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0x01 => {
                    self.ora(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0x11 => {
                    self.ora(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }

                //ASL
                0x0A => {
                    self.asl_with_a();
                }
                0x06 => {
                    self.asl_with_memory(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x16 => {
                    self.asl_with_memory(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x0E => {
                    self.asl_with_memory(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x1E => {
                    self.asl_with_memory(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }

                // LSR
                0x4A => {
                    self.lsr_with_a();
                }
                0x46 => {
                    self.lsr_with_memory(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x56 => {
                    self.lsr_with_memory(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x4E => {
                    self.lsr_with_memory(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x5E => {
                    self.lsr_with_memory(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }

                // ROL
                0x2A => {
                    self.rol_with_a();
                }
                0x26 => {
                    self.rol_with_memory(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x36 => {
                    self.rol_with_memory(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x2E => {
                    self.rol_with_memory(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x3E => {
                    self.rol_with_memory(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }

                // ROR
                0x6A => {
                    self.ror_with_a();
                }
                0x66 => {
                    self.ror_with_memory(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x76 => {
                    self.ror_with_memory(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x6E => {
                    self.ror_with_memory(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x7E => {
                    self.ror_with_memory(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }

                // INC
                0xE6 => {
                    self.inc(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xF6 => {
                    self.inc(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0xEE => {
                    self.inc(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xFE => {
                    self.inc(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }

                // INX
                0xE8 => {
                    self.inx();
                }
                // INY
                0xC8 => {
                    self.iny();
                }

                // DEC
                0xC6 => {
                    self.dec(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xD6 => {
                    self.dec(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0xCE => {
                    self.dec(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xDE => {
                    self.dec(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }

                // DEX
                0xCA => {
                    self.dex();
                }
                // DEY
                0x88 => {
                    self.dey();
                }

                // CMP
                0xC9 => {
                    self.cmp(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0xC5 => {
                    self.cmp(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xD5 => {
                    self.cmp(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0xCD => {
                    self.cmp(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xDD => {
                    self.cmp(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0xD9 => {
                    self.cmp(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0xC1 => {
                    self.cmp(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0xD1 => {
                    self.cmp(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }

                // CPX
                0xE0 => {
                    self.cpx(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0xE4 => {
                    self.cpx(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xEC => {
                    self.cpx(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                // CPY
                0xC0 => {
                    self.cpy(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0xC4 => {
                    self.cpy(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xCC => {
                    self.cpy(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }

                // JMP
                0x4C => {
                    self.jmp_absolute();
                    // jmpしてるのでPCの加算は不要
                    // self.program_counter += 2;
                }
                0x6C => {
                    self.jmp_indirect();
                    // jmpしてるのでPCの加算は不要
                    // self.program_counter += 2;
                }

                // JSR
                0x20 => {
                    self.jsr();
                }
                // RTS
                0x60 => {
                    self.rts();
                }

                // RTI
                0x40 => {
                    self.rti();
                }
                // bne
                0xD0 => {
                    self.bne();
                }
                // bvs
                0x70 => {
                    self.bvs();
                }
                // bvc
                0x50 => {
                    self.bvc();
                }
                // bmi
                0x30 => {
                    self.bmi();
                }
                // beq
                0xF0 => {
                    self.beq();
                }
                // bcs
                0xB0 => {
                    self.bcs();
                }
                // bcc
                0x90 => {
                    self.bcc();
                }
                // bpl
                0x10 => {
                    self.bpl();
                }

                // bit
                0x24 => {
                    self.bit(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                // bit
                0x2c => {
                    self.bit(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }

                // LDA
                0xA9 => {
                    self.lda(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0xA5 => {
                    self.lda(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xAD => {
                    self.lda(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xA1 => {
                    self.lda(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0xB1 => {
                    self.lda(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }
                0xB5 => {
                    self.lda(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0xBD => {
                    self.lda(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0xB9 => {
                    self.lda(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }

                // LDX
                0xA2 => {
                    self.ldx(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0xA6 => {
                    self.ldx(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xB6 => {
                    self.ldx(&AddressingMode::ZeroPage_Y);
                    self.program_counter += 1;
                }
                0xAE => {
                    self.ldx(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xBE => {
                    self.ldx(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }

                // LDY
                0xA0 => {
                    self.ldy(&AddressingMode::Immediate);
                    self.program_counter += 1;
                }
                0xA4 => {
                    self.ldy(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xB4 => {
                    self.ldy(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0xAC => {
                    self.ldy(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xBC => {
                    self.ldy(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }

                // STA
                0x85 => {
                    self.sta(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x95 => {
                    self.sta(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x8D => {
                    self.sta(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0x9D => {
                    self.sta(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0x99 => {
                    self.sta(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0x81 => {
                    self.sta(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0x91 => {
                    self.sta(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }

                // STX
                0x86 => {
                    self.stx(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x96 => {
                    self.stx(&AddressingMode::ZeroPage_Y);
                    self.program_counter += 1;
                }
                0x8E => {
                    self.stx(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }

                // STY
                0x84 => {
                    self.sty(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0x94 => {
                    self.sty(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0x8C => {
                    self.sty(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }

                // CLD/SED
                // NOTE: BCDモードから通常モードに戻ります。ファミコンでは実装されていません。[0.0.0.0.D.0.0.0]
                0xF8 => {
                    self.sed();
                }
                0xD8 => {
                    self.cld();
                }

                // CLI
                0x58 => {
                    self.cli();
                }
                // SEI
                0x78 => {
                    self.sei();
                }

                // CLV
                0xB8 => {
                    self.clv();
                }

                // CLC
                0x18 => {
                    self.clc();
                }
                // SEC
                0x38 => {
                    self.sec();
                }

                // TAX
                0xAA => {
                    self.tax();
                }
                // TAY
                0xA8 => {
                    self.tay();
                }

                // TSX
                0xBA => {
                    self.tsx();
                }
                // TXA
                0x8A => {
                    self.txa();
                }
                // TXS
                0x9A => {
                    self.txs();
                }
                // TYA
                0x98 => {
                    self.tya();
                }

                // PHA
                0x48 => {
                    self.pha();
                }
                // PLA
                0x68 => {
                    self.pla();
                }
                // PHP
                0x08 => {
                    self.php();
                }
                // PLP
                0x28 => {
                    self.plp();
                }

                0x00 => {
                    self.debug("game end".to_string());
                    return;
                }

                // NOP
                0x04 | 0x44 | 0x64 => {
                    let (addr, page_cross) = self.get_operand_address(&AddressingMode::ZeroPage);
                    let data = self.memory_read(addr);
                    if page_cross {
                        self.bus.tick(1);
                    }
                }
                0x14 | 0x34 | 0x54 | 0x74 | 0xd4 | 0xf4 => {
                    let (addr, page_cross) = self.get_operand_address(&AddressingMode::ZeroPage_X);
                    let data = self.memory_read(addr);
                    if page_cross {
                        self.bus.tick(1);
                    }
                }
                0x0c => {
                    let (addr, page_cross) = self.get_operand_address(&AddressingMode::Absolute);
                    let data = self.memory_read(addr);
                    if page_cross {
                        self.bus.tick(1);
                    }
                }
                0x1c | 0x3c | 0x5c | 0x7c | 0xdc | 0xfc => {
                    let (addr, page_cross) = self.get_operand_address(&AddressingMode::Absolute_X);
                    let data = self.memory_read(addr);
                    if page_cross {
                        self.bus.tick(1);
                    }
                }
                0x02 | 0x12 | 0x22 | 0x32 | 0x42 | 0x52 | 0x62 | 0x72 | 0x92 | 0xb2 | 0xd2
                | 0xf2 => { /* do nothing */ }
                0x1a | 0x3a | 0x5a | 0x7a | 0xda | 0xfa => { /* do nothing */ }
                // SKB
                0x80 | 0x82 | 0x89 | 0xc2 | 0xe2 => {}

                /* LAX */
                0xa7 => {
                    self.lax(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xb7 => {
                    self.lax(&AddressingMode::ZeroPage_Y);
                    self.program_counter += 1;
                }
                0xaf => {
                    self.lax(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xbf => {
                    self.lax(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0xa3 => {
                    self.lax(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0xb3 => {
                    self.lax(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }

                /* SAX */
                0x87 => {
                    let data = self.register_a & self.register_x;
                    let (addr, _) = self.get_operand_address(&AddressingMode::ZeroPage);
                    self.memory_write(addr, data);
                    self.program_counter += 1;
                }
                0x97 => {
                    let data = self.register_a & self.register_x;
                    let (addr, _) = self.get_operand_address(&AddressingMode::ZeroPage_Y);
                    self.memory_write(addr, data);
                    self.program_counter += 1;
                }
                0x8f => {
                    let data = self.register_a & self.register_x;
                    let (addr, _) = self.get_operand_address(&AddressingMode::Absolute);
                    self.memory_write(addr, data);
                    self.program_counter += 2;
                }
                0x83 => {
                    let data = self.register_a & self.register_x;
                    let (addr, _) = self.get_operand_address(&AddressingMode::Indirect_X);
                    self.memory_write(addr, data);
                    self.program_counter += 1;
                }

                // DCP
                0xc7 => {
                    self.dcp(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                }
                0xd7 => {
                    self.dcp(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                }
                0xCF => {
                    self.dcp(&AddressingMode::Absolute);
                    self.program_counter += 2;
                }
                0xdF => {
                    self.dcp(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                }
                0xdb => {
                    self.dcp(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                }
                0xc3 => {
                    self.dcp(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                }
                0xd3 => {
                    self.dcp(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;
                }


                /* ISB */
                0xe7 => {
                    self.inc(&AddressingMode::ZeroPage);
                    self.program_counter += 1;
                } 0xf7 => {
                    self.inc(&AddressingMode::ZeroPage_X);
                    self.program_counter += 1;
                } 0xef => {
                    self.inc(&AddressingMode::Absolute);
                    self.program_counter += 2;
                } 0xff => {
                    self.inc(&AddressingMode::Absolute_X);
                    self.program_counter += 2;
                } 
                0xfb => {
                    self.inc(&AddressingMode::Absolute_Y);
                    self.program_counter += 2;
                } 
                0xe3 => {
                    self.inc(&AddressingMode::Indirect_X);
                    self.program_counter += 1;
                } 0xf3 => {
                    self.inc(&AddressingMode::Indirect_Y);
                    self.program_counter += 1;   
                }

                x => {
                    panic!("what opcode!? {}", x);
                }
            }

            let _opcode = opcodes.get(&opscode).unwrap();
            self.bus.tick(_opcode.cycles);

            // // WHY?
            // if program_counter_state == self.program_counter {
            //     self.program_counter += (_opcode.len -1 ) as u16;
            // }
        }
    }

    fn get_operand_address(&mut self, mode: &AddressingMode) -> (u16, bool) {
        match mode {
            AddressingMode::Immediate => (self.program_counter, false),

            AddressingMode::ZeroPage => (self.memory_read(self.program_counter) as u16, false),

            AddressingMode::Absolute => (self.memory_read_u16(self.program_counter), false),

            AddressingMode::ZeroPage_X => {
                // ZeroPageにレジスタXを加算
                let pos = self.memory_read(self.program_counter);
                let addr = pos.wrapping_add(self.register_x) as u16;
                (addr, false)
            }
            AddressingMode::ZeroPage_Y => {
                // ZeroPageにレジスタYを加算
                let pos = self.memory_read(self.program_counter);
                let addr = pos.wrapping_add(self.register_y) as u16;
                (addr, false)
            }

            AddressingMode::Absolute_X => {
                let base = self.memory_read_u16(self.program_counter);
                let addr = base.wrapping_add(self.register_x as u16);
                (addr, page_cross(base, addr))
            }
            AddressingMode::Absolute_Y => {
                let base = self.memory_read_u16(self.program_counter);
                let addr = base.wrapping_add(self.register_y as u16);
                (addr, page_cross(base, addr))
            }

            AddressingMode::Indirect_X => {
                let base = self.memory_read(self.program_counter);

                let ptr: u8 = (base as u8).wrapping_add(self.register_x);
                let lo = self.memory_read(ptr as u16);
                let hi = self.memory_read(ptr.wrapping_add(1) as u16);
                ((hi as u16) << 8 | (lo as u16), false)
            }
            AddressingMode::Indirect_Y => {
                let base = self.memory_read(self.program_counter);

                let lo = self.memory_read(base as u16);
                let hi = self.memory_read((base as u8).wrapping_add(1) as u16);
                let deref_base = (hi as u16) << 8 | (lo as u16);
                let deref = deref_base.wrapping_add(self.register_y as u16);
                (deref, page_cross(deref, deref_base))
            }

            AddressingMode::NoneAddressing => {
                panic!("mode {:?} is not supported", mode);
            }
        }
    }

    pub fn get_absolute_address(&mut self, mode: &AddressingMode, addr: u16) -> u16 {
        match mode {
            AddressingMode::ZeroPage => self.memory_read(addr) as u16,

            AddressingMode::Absolute => self.memory_read_u16(addr),

            AddressingMode::ZeroPage_X => {
                let pos = self.memory_read(addr);
                let addr = pos.wrapping_add(self.register_x) as u16;
                addr
            }
            AddressingMode::ZeroPage_Y => {
                let pos = self.memory_read(addr);
                let addr = pos.wrapping_add(self.register_y) as u16;
                addr
            }

            AddressingMode::Absolute_X => {
                let base = self.memory_read_u16(addr);
                let addr = base.wrapping_add(self.register_x as u16);
                addr
            }
            AddressingMode::Absolute_Y => {
                let base = self.memory_read_u16(addr);
                let addr = base.wrapping_add(self.register_y as u16);
                addr
            }

            AddressingMode::Indirect_X => {
                let base = self.memory_read(addr);

                let ptr: u8 = (base as u8).wrapping_add(self.register_x);
                let lo = self.memory_read(ptr as u16);
                let hi = self.memory_read(ptr.wrapping_add(1) as u16);
                (hi as u16) << 8 | (lo as u16)
            }
            AddressingMode::Indirect_Y => {
                let base = self.memory_read(addr);

                let lo = self.memory_read(base as u16);
                let hi = self.memory_read((base as u8).wrapping_add(1) as u16);
                let deref_base = (hi as u16) << 8 | (lo as u16);
                let deref = deref_base.wrapping_add(self.register_y as u16);
                deref
            }

            _ => {
                panic!("mode {:?} is not supported", mode);
            }
        }
    }

    fn debug(&mut self, label: String) {
        //_log::debug!("{:20}... code: {:#06x}[] a: {:#06x} x: {:#06x} y: {:#06x} pc: {:#06x} sp: {:#06x} status: {:#10b}", label, self.memory_read(self.program_counter), self.register_a, self.register_x, self.register_y, self.program_counter, self.stack_pointer, self.status);
    }
}

fn page_cross(addr1: u16, addr2: u16) -> bool {
    addr1 & 0xFF00 != addr2 & 0xFF00
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    // fn test_0xa9_lda_immidiate_load_data() {
    //     let mut cpu = CPU::new();
    //     cpu.load_and_run(vec![0xa9, 0x05, 0x00]);
    //     assert_eq!(cpu.register_a, 0x05);
    //     assert!(cpu.status & 0b0000_0010 == 0b00);
    //     assert!(cpu.status & 0b1000_0000 == 0);
    // }

    // #[test]
    // fn test_0xa9_lda_zero_flag() {
    //     let mut cpu = CPU::new();
    //     cpu.load_and_run(vec![0xa9, 0x00, 0x00]);
    //     assert!(cpu.status & 0b0000_0010 == 0b10);
    // }

    // #[test]
    // fn test_0xaa_tax_move_a_to_x() {
    //     let mut cpu = CPU::new();
    //     cpu.load(vec![0xAA, 0x00]);
    //     cpu.reset();
    //     cpu.register_a = 10;
    //     cpu.run();

    //     assert_eq!(cpu.register_x, 10)
    // }

    // #[test]
    // fn test_5_ops_working_together() {
    //     let mut cpu = CPU::new();
    //     cpu.load_and_run(vec![0xa9, 0xc0, 0xaa, 0xe8, 0x00]);

    //     assert_eq!(cpu.register_x, 0xc1)
    // }

    // #[test]
    // fn test_inx_overflow() {
    //     let mut cpu = CPU::new();
    //     cpu.load(vec![0xe8, 0xe8, 0x00]);
    //     cpu.reset();
    //     cpu.register_x = 0xff;
    //     cpu.run();

    //     assert_eq!(cpu.register_x, 1)
    // }

    // #[test]
    // fn test_lda_from_memory() {
    //     let mut cpu = CPU::new();
    //     cpu.memory_write(0x10, 0x55);

    //     // NOTE: 0x10の値をAレジスタにロード
    //     cpu.load_and_run(vec![0xa5, 0x10, 0x00]);

    //     assert_eq!(cpu.register_a, 0x55);
    // }
    #[test]
    fn test_bit_operations() {
        assert_eq!(0b1010_10100 | 1, 0b1010_10101);
        assert_eq!(0b1010_10101 & 1, 0b1);

        assert_eq!(0b1010_10100 | 0b0100_00000, 0b1110_10100);
        assert_eq!(0b1010_10100 & 0b0010_00000, 0b0010_00000);
    }

    #[test]
    fn test_bit_slide_and_rotate() {
        assert_eq!((0b1111_1111 as u8) << 2, 0b1111_1100);
        assert_eq!(0b1111_1111 << 2, 0b11_1111_1100);

        assert_eq!((0b0001_0001 as u8).rotate_left(1), 0b0010_0010);
        assert_eq!((0b0001_0001 as u8).rotate_left(2), 0b0100_0100);

        assert_eq!(!(1 as u8), 0b1111_1110);
    }
}
