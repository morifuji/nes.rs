use crate::{cartridge::Rom, ppu::PPU, joypad::Joypad};

const RAM: u16 = 0x0000;
const RAM_MIRRORS_END: u16 = 0x1FFF;
const PPU_REGISTERS_MIRROR_END:u16 = 0x3FFF;


impl<'a> Bus<'a>  {
    pub fn mem_read(&mut self, addr: u16) -> u8 {
        match addr {
            RAM ..= RAM_MIRRORS_END => {
                let mirror_down_addr = addr & 0b111_1111_1111;
                self.cpu_vram[mirror_down_addr as usize]
            }
            0x2000 | 0x2001 | 0x2003 | 0x2005 | 0x2006 | 0x4014 => {
                // panic!("can't read to write-only address! {}", addr);
                0
            },
            0x2002 => self.ppu.read_status(),
            0x2004 => self.ppu.read_to_oam_data(),
            0x2007 => self.ppu.read_data(),
            0x2008 ..= PPU_REGISTERS_MIRROR_END => {
                // WHY?
                // PPUレジスタのミラーリングの記述が見つからない
                let mirror_down_adddress = addr & 0b00000111_11111111;
                self.mem_read(mirror_down_adddress)
            },
            0x4016 => {
                self.joypad1.read()
            },

            0x8000 ..= 0xFFFF => {
                self.read_prg_rom(addr)
            }            
            _ => {
                //_log::debug!("Ignoring mem access read {}", addr);
                0
            }
        }
    }

    pub fn mem_write(&mut self, addr: u16, data: u8)  {
        //_log::debug!("mem_write {:#10x}", addr);
        match addr {
            RAM ..= RAM_MIRRORS_END => {
                let mirror_down_addr = addr & 0b111_1111_1111;
                self.cpu_vram[mirror_down_addr as usize] = data;
            }
            0x2000 => {
                self.ppu.write_to_ctrl(data)
            }
            0x2001 => {
                self.ppu.write_to_mask(data)
            }
            0x2002 => {
                panic!("attempt to write to PPU status register")
                // self.ppu.write_to_status(data)
            }
            0x2003 => {
                self.ppu.write_to_oam_addr(data);
            }
            0x2004 => {
                self.ppu.write_to_oam_data(data);
            }
            0x2005 => {
                self.ppu.write_to_scroll(data);
            }
            0x2006 => {
                self.ppu.write_to_ppu_addr(data)
            }
            0x2007 => {
                self.ppu.write_data(data)
            }
            0x2008 ..= PPU_REGISTERS_MIRROR_END => {
                let mirror_down_adddress = addr & 0b00100000_00000111;
                // let mirror_down_adddress = addr & 0b00000111_11111111;
                self.mem_write(mirror_down_adddress, data)
            }
            0x8000..=0xFFFF => panic!("Attempt to write to Cartridge ROM space: {:x}", addr),
            0x4014 => {
                let mut buffer: [u8; 256] = [0; 256];
                let hi: u16 = (data as u16) << 8;
                for i in 0..256u16 {
                    buffer[i as usize] = self.mem_read(hi + i);
                }

                self.ppu.write_to_oam_dma(&buffer);
            },
            0x4016 => {
                self.joypad1.write(data);
            },
            _ => {
                //_log::debug!("Ignoring mem access write {}", addr);
            }
        }
    }

    fn read_prg_rom(&self, mut addr: u16) -> u8 {
        addr -= 0x8000;
        if self.prg_rom.len() == 0x4000 && addr >= 0x4000 {
            //mirror if needed
            addr = addr % 0x4000;
        }
        self.prg_rom[addr as usize]
    }

    pub fn new<'call, F>(rom: Rom, gameloop_callback: F) -> Bus<'call>
    where F: FnMut(&PPU, &mut Joypad) + 'call,
    {
        let ppu = PPU::new(rom.chr_rom, rom.screen_mirroring);

        Bus {
            prg_rom: rom.prg_rom,
            cpu_vram: [0; 2048],
            ppu,
            cycles: 0,
            gameloop_callback: Box::from(gameloop_callback),
            joypad1: Joypad::new()
        }
    }

    pub fn poll_nmi_status(&mut self) -> Option<u8>{
        // 取得して同時にnoneに更新
        self.ppu.nmi_interrupt.take()
    }

    pub fn tick(&mut self, cycles: u8) {
        //_log::debug!("bus TICK: {:?}/{:?}", self.cycles, cycles);
        self.cycles += cycles as usize;

        let nmi_before = self.ppu.nmi_interrupt.is_some();
        self.ppu.tick(cycles*3);
        let nmi_after = self.ppu.nmi_interrupt.is_some();

        // //_log::debug!("loopback!");
        if !nmi_before && nmi_after {
            //_log::debug!("RENDER!");
            (self.gameloop_callback)(&self.ppu, &mut self.joypad1);
        }
    }
 }

pub struct Bus<'call> {
    cpu_vram: [u8; 2048],
    prg_rom: Vec<u8>,
    ppu: PPU,

    // 現在のサイクル数
    cycles: usize,

    // NOTE: https://users.rust-lang.org/t/how-to-implement-debug-for-dyn-trait/39950
    gameloop_callback: Box<dyn FnMut(&PPU, &mut Joypad) + 'call>,

    joypad1: Joypad
 }
 



 #[cfg(test)]
mod test {
    use super::*;
    use crate::cartridge::test;

    #[test]
    fn test_mem_read_write_to_ram() {
        let mut bus = Bus::new(test::test_rom(), |ppu: &PPU, joypad| {});
        bus.mem_write(0x01, 0x55);
        assert_eq!(bus.mem_read(0x01), 0x55);
    }
}
