
use bitflags::bitflags;
bitflags! {
    pub struct MaskRegister: u8 {
        // NOTE: https://taotao54321.hatenablog.com/entry/2017/04/11/115205
        const EMPHASIS_BLUE = 0b1000_0000;
        const EMPHASIS_GREEN = 0b0100_0000;
        const EMPHASIS_RED = 0b0010_0000;
        const SPRITE = 0b0001_0000;

        const BG = 0b0000_1000;
        const LEFT_SPRITE_CLIPPING = 0b0000_0100;
        const RIGHT_SPRITE_CLIPPING = 0b0000_0010;
        const COLOR = 0b0000_0001;
    }
}

impl MaskRegister {
    pub fn new() -> Self {
        MaskRegister::from_bits_truncate(0b0000_0000)
    }

    pub fn update(&mut self, data: u8) {
        self.bits = data;
    }
}