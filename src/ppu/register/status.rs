
use bitflags::bitflags;
bitflags! {
    pub struct StatusRegister: u8 {
        // NOTE: https://taotao54321.hatenablog.com/entry/2017/04/11/115205
        const VBLANK = 0b1000_0000;
        const SPRITE_0_HIT = 0b0100_0000;
        const SPRITE_OVERFLOW = 0b0010_0000;
    }
}

impl StatusRegister {
    pub fn new() -> Self {
        StatusRegister::from_bits_truncate(0b0000_0000)
    }

    pub fn update(&mut self, data: u8) {
        self.bits = data;
    }


    pub fn set_vblank_status(&mut self, status: bool) {
        self.set(StatusRegister::VBLANK, status)
    }

    pub fn reset_vblank_status(&mut self) {
        self.set(StatusRegister::VBLANK, false)
    }

    pub fn snapshot(&self) -> u8 {
        self.bits
    }
}