#[derive(Debug)]
pub struct ScrollRegister {
    offset_x: u8,
    offset_y: u8,
    is_clear: bool
}

impl ScrollRegister {
    pub fn new() -> Self {
        ScrollRegister { offset_x:0,offset_y: 0, is_clear: false }
    }

    pub fn update(&mut self, data: u8) {
        if self.is_clear {
            self.offset_x = data;
        } else {
            self.offset_y = data;
        }

        self.is_clear = !self.is_clear;
    }

    pub fn reset_latch(&mut self) {
        self.is_clear = false;
    }
}