use crate::cartridge::Mirroring;

use self::register::{mask::MaskRegister, status::StatusRegister, scroll::ScrollRegister, addr::AddrRegister, control::ControlRegister};

pub mod register;

const CYCLES_IN_SCANLINE: usize = 341;
const CYCLE_NMI: usize = 241;

const SCAN_LINE_IN_FRAME: u16 = 262;
#[derive(Debug)]
pub struct PPU {
    // 画面表示のプログラム
    pub chr_rom: Vec<u8>,
    // 0x3f00 ~ 0x3fff
    pub palette_table: [u8; 32],
    // 背景、2KiB
    pub vram: [u8; 2048],
    // スプライトの状態
    pub oam_data: [u8; 256],
    pub oam_addr: u8,

    pub mirroring: Mirroring,

    // 0x2006: Address
    addr: AddrRegister,

    // 0x2000: Control
    pub ctrl: ControlRegister,

    mask: MaskRegister,
    status: StatusRegister,
    scroll: ScrollRegister,

    // ???
    internal_data_buf: u8,

    // 処理行数
    scanline: u16,
    cycles: usize,

    // NMI割り込みが発生したかどうか
    // CPUに知らせるためだけに仮想的に用意された変数
    pub nmi_interrupt: Option<u8>
}

impl PPU {
    pub fn new(chr_rom: Vec<u8>, mirroring: Mirroring) -> PPU {
        PPU {
            chr_rom,
            palette_table: [0; 32],
            vram: [0; 2048],
            oam_data: [0; 256],
            oam_addr: 0,
            mirroring,
            addr: AddrRegister::new(),
            ctrl: ControlRegister::new(),
            mask: MaskRegister::new(),
            status: StatusRegister::new(),
            scroll: ScrollRegister::new(),
            internal_data_buf: 0,
            scanline: 0,
            cycles: 0,
            nmi_interrupt: None
        }
    }

    pub fn write_to_ppu_addr(&mut self, value: u8) {
        //_log::debug!("PPU/write_to_ppu_addr {:?}", value);
        self.addr.update(value);
    }

    pub fn write_to_ctrl(&mut self, data: u8) {
        //_log::debug!("PPU/write_to_ctrl {:?}", data);
        let before_nmi_status = self.ctrl.generate_vblank_nmi();
        self.ctrl.update(data);

        if !before_nmi_status && self.ctrl.generate_vblank_nmi() && self.status.contains(StatusRegister::VBLANK) {
            self.nmi_interrupt = Some(1);
        }
    }
    pub fn write_to_mask(&mut self, data: u8) {
        self.mask.update(data);
    }
    // pub fn write_to_status(&mut self, data: u8) {
    //     self.status.update(data);
    // }

    pub fn write_to_oam_addr(&mut self, data: u8) {
        //_log::debug!("PPU/write_to_oam_addr {:?}", data);
        self.oam_addr = data;
    }
    pub fn write_to_oam_data(&mut self, data: u8) {
        //_log::debug!("PPU/write_to_oam_data {:?}", data);
        self.oam_data[self.oam_addr as usize] = data;

        // 1つずらす
        self.oam_addr = self.oam_addr.wrapping_add(1);
    }
    pub fn write_to_scroll(&mut self, data: u8) {
        //_log::debug!("PPU/write_to_scroll {:?}", data);
        self.scroll.update(data);
    }

    pub fn write_to_oam_dma(&mut self, data: &[u8; 256]) {
        for x in data.iter() {
            self.oam_data[self.oam_addr as usize] = *x;
            self.oam_addr = self.oam_addr.wrapping_add(1);
        }
    }

    fn increment_vram_addr(&mut self) {
        self.addr.increment(self.ctrl.vram_addr_increment());
    }

    pub fn read_data(&mut self) -> u8 {
        //_log::debug!("PPU/read_data");
        let addr = self.addr.get();

        self.increment_vram_addr();

        match addr {
            0..=0x1fff => {
                // CHR_ROMにバイパスするだけ
                let result = self.internal_data_buf;
                self.internal_data_buf = self.chr_rom[addr as usize];
                result
            }
            0x2000..=0x2fff => {
                // VRAMからバイパスするだけ
                let result = self.internal_data_buf;
                self.internal_data_buf = self.vram[self.mirror_vram_addr(addr) as usize];
                result
            }
            // $3000-$3EFF	通常は $2000-$2EFF ミラー
            0x3000..=0x3eff => panic!(
                "addr space 0x3000..0x3eff is not expected to be used, requested = {} ",
                addr
            ),
            0x3f00..=0x3fff => self.palette_table[(addr - 0x3fff) as usize],
            _ => panic!("unexpected access to mirrored space {}", addr),
        }
    }

    pub fn read_to_oam_data(&mut self) -> u8{
        self.oam_data[self.oam_addr as usize]
    }
    pub fn read_status(&mut self) -> u8{
        //_log::debug!("PPU/read_status");
        let data = self.status.snapshot();
        self.status.reset_vblank_status();
        self.addr.reset_latch();
        self.scroll.reset_latch();
        data
    }

    pub fn write_data(&mut self, data: u8) {
        //_log::debug!("PPU/write_to_ppu_addr {:?}", data);
        let addr = self.addr.get();

        match addr {
            0..=0x1fff => {
                // 何もしない
            }
            0x2000..=0x2fff => {
                // VRAMからバイパスするだけ
                self.vram[self.mirror_vram_addr(addr) as usize] = data;
                // log::debug!("{:?}", self.vram);
                // log::debug!("{:?}", data);
                println!("{:?}/{:?}", self.mirror_vram_addr(addr), data);
            }
            // $3000-$3EFF	通常は $2000-$2EFF ミラー
            0x3f10 | 0x3f14 | 0x3f18 | 0x3f1c => {
                let add_mirror = addr - 0x10;
                self.palette_table[(add_mirror - 0x3f00) as usize] = data;
            },
            0x3000..=0x3eff => panic!(
                "addr space 0x3000..0x3eff is not expected to be used, requested = {} ",
                addr
            ),
            0x3f00..=0x3fff => {
                self.palette_table[(addr - 0x3f00) as usize] = data;
            }
            _ => panic!("unexpected access to mirrored space {}", addr),
        }

        self.increment_vram_addr();
    }

    // Horizontal:
    //   [ A ] [ a ]
    //   [ B ] [ b ]

    // Vertical:
    //   [ A ] [ B ]
    //   [ a ] [ b ]
    pub fn mirror_vram_addr(&self, addr: u16) -> u16 {
        let mirrored_vram = addr & 0b10111111111111; // mirror down 0x3000-0x3eff to 0x2000 - 0x2eff
        let vram_index = mirrored_vram - 0x2000; // to vram vector
        let name_table = vram_index / 0x400; // to the name table index
        match (&self.mirroring, name_table) {
            (Mirroring::VERTICAL, 2) | (Mirroring::VERTICAL, 3) => vram_index - 0x800,
            (Mirroring::HORIZONTAL, 2) => vram_index - 0x400,
            (Mirroring::HORIZONTAL, 1) => vram_index - 0x400,
            (Mirroring::HORIZONTAL, 3) => vram_index - 0x800,
            _ => vram_index,
        }
    }


    pub fn tick(&mut self, cycles: u8) -> bool {
        self.cycles += cycles as usize;

        //_log::debug!("CYCLE: {:?}", self.cycles);

        if self.cycles >= CYCLES_IN_SCANLINE {
            self.cycles -= CYCLES_IN_SCANLINE;
            self.scanline += 1;

            if self.scanline == (CYCLE_NMI as u16) {
                self.status.set_vblank_status(true);
                // self.status.set_sprite_zero_hit(false);
                // //_log::debug!("LINE {} {}", self.scanline, self.ctrl.generate_vblank_nmi());

                if self.ctrl.generate_vblank_nmi() {
                    self.nmi_interrupt = Some(1);
                }
            }

            if self.scanline >= SCAN_LINE_IN_FRAME {
                self.scanline = 0;
                self.status.reset_vblank_status();
                return true;
            }
        }

        return false;
    }

    // for TEST ONLY
    pub fn new_empty_rom() -> Self {
        PPU::new(vec![0; 2048], Mirroring::HORIZONTAL)
    }


}



#[cfg(test)]
pub mod test {
    use super::*;

    #[test]
    fn test_ppu_vram_writes() {
        let mut ppu = PPU::new_empty_rom();
        ppu.write_to_ppu_addr(0x23);
        ppu.write_to_ppu_addr(0x05);
        ppu.write_data(0x66);

        assert_eq!(ppu.vram[0x0305], 0x66);
    }

    #[test]
    fn test_ppu_vram_reads() {
        let mut ppu = PPU::new_empty_rom();
        ppu.write_to_ctrl(0);
        ppu.vram[0x0305] = 0x66;

        ppu.write_to_ppu_addr(0x23);
        ppu.write_to_ppu_addr(0x05);

        ppu.read_data(); //load_into_buffer
        assert_eq!(ppu.addr.get(), 0x2306);
        assert_eq!(ppu.read_data(), 0x66);
    }

    #[test]
    fn test_ppu_vram_reads_cross_page() {
        let mut ppu = PPU::new_empty_rom();
        ppu.write_to_ctrl(0);
        ppu.vram[0x01ff] = 0x66;
        ppu.vram[0x0200] = 0x77;

        ppu.write_to_ppu_addr(0x21);
        ppu.write_to_ppu_addr(0xff);

        ppu.read_data(); //load_into_buffer
        assert_eq!(ppu.read_data(), 0x66);
        assert_eq!(ppu.read_data(), 0x77);
    }

    #[test]
    fn test_ppu_vram_reads_step_32() {
        let mut ppu = PPU::new_empty_rom();
        ppu.write_to_ctrl(0b100);
        ppu.vram[0x01ff] = 0x66;
        ppu.vram[0x01ff + 32] = 0x77;
        ppu.vram[0x01ff + 64] = 0x88;

        ppu.write_to_ppu_addr(0x21);
        ppu.write_to_ppu_addr(0xff);

        ppu.read_data(); //load_into_buffer
        assert_eq!(ppu.read_data(), 0x66);
        assert_eq!(ppu.read_data(), 0x77);
        assert_eq!(ppu.read_data(), 0x88);
    }

    // Horizontal: https://wiki.nesdev.com/w/index.php/Mirroring
    //   [0x2000 A ] [0x2400 a ]
    //   [0x2800 B ] [0x2C00 b ]
    #[test]
    fn test_vram_horizontal_mirror() {
        let mut ppu = PPU::new_empty_rom();
        ppu.write_to_ppu_addr(0x24);
        ppu.write_to_ppu_addr(0x05);

        ppu.write_data(0x66); //write to a

        ppu.write_to_ppu_addr(0x28);
        ppu.write_to_ppu_addr(0x05);

        ppu.write_data(0x77); //write to B

        ppu.write_to_ppu_addr(0x20);
        ppu.write_to_ppu_addr(0x05);

        ppu.read_data(); //load into buffer
        assert_eq!(ppu.read_data(), 0x66); //read from A

        ppu.write_to_ppu_addr(0x2C);
        ppu.write_to_ppu_addr(0x05);

        ppu.read_data(); //load into buffer
        assert_eq!(ppu.read_data(), 0x77); //read from b
    }

    // Vertical: https://wiki.nesdev.com/w/index.php/Mirroring
    //   [0x2000 A ] [0x2400 B ]
    //   [0x2800 a ] [0x2C00 b ]
    #[test]
    fn test_vram_vertical_mirror() {
        let mut ppu = PPU::new(vec![0; 2048], Mirroring::VERTICAL);

        ppu.write_to_ppu_addr(0x20);
        ppu.write_to_ppu_addr(0x05);

        ppu.write_data(0x66); //write to A

        ppu.write_to_ppu_addr(0x2C);
        ppu.write_to_ppu_addr(0x05);

        ppu.write_data(0x77); //write to b

        ppu.write_to_ppu_addr(0x28);
        ppu.write_to_ppu_addr(0x05);

        ppu.read_data(); //load into buffer
        assert_eq!(ppu.read_data(), 0x66); //read from a

        ppu.write_to_ppu_addr(0x24);
        ppu.write_to_ppu_addr(0x05);

        ppu.read_data(); //load into buffer
        assert_eq!(ppu.read_data(), 0x77); //read from B
    }

    #[test]
    fn test_read_status_resets_latch() {
        let mut ppu = PPU::new_empty_rom();
        ppu.vram[0x0305] = 0x66;

        ppu.write_to_ppu_addr(0x21);
        ppu.write_to_ppu_addr(0x23);
        ppu.write_to_ppu_addr(0x05);

        ppu.read_data(); //load_into_buffer
        assert_ne!(ppu.read_data(), 0x66);

        ppu.read_status();

        ppu.write_to_ppu_addr(0x23);
        ppu.write_to_ppu_addr(0x05);

        ppu.read_data(); //load_into_buffer
        assert_eq!(ppu.read_data(), 0x66);
    }

    #[test]
    fn test_ppu_vram_mirroring() {
        let mut ppu = PPU::new_empty_rom();
        ppu.write_to_ctrl(0);
        ppu.vram[0x0305] = 0x66;

        ppu.write_to_ppu_addr(0x63); //0x6305 -> 0x2305
        ppu.write_to_ppu_addr(0x05);

        ppu.read_data(); //load into_buffer
        assert_eq!(ppu.read_data(), 0x66);
        // assert_eq!(ppu.addr.read(), 0x0306)
    }

    #[test]
    fn test_read_status_resets_vblank() {
        let mut ppu = PPU::new_empty_rom();
        ppu.status.set_vblank_status(true);

        let status = ppu.read_status();

        assert_eq!(status >> 7, 1);
        assert_eq!(ppu.status.snapshot() >> 7, 0);
    }

    #[test]
    fn test_oam_read_write() {
        let mut ppu = PPU::new_empty_rom();
        ppu.write_to_oam_addr(0x10);
        ppu.write_to_oam_data(0x66);
        ppu.write_to_oam_data(0x77);

        ppu.write_to_oam_addr(0x10);
        assert_eq!(ppu.read_to_oam_data(), 0x66);

        ppu.write_to_oam_addr(0x11);
        assert_eq!(ppu.read_to_oam_data(), 0x77);
    }

    // #[test]
    // fn test_oam_dma() {
    //     let mut ppu = PPU::new_empty_rom();

    //     let mut data = [0x66; 256];
    //     data[0] = 0x77;
    //     data[255] = 0x88;

    //     ppu.write_to_oam_addr(0x10);
    //     ppu.write_oam_dma(&data);

    //     ppu.write_to_oam_addr(0xf); //wrap around
    //     assert_eq!(ppu.read_to_oam_data(), 0x88);

    //     ppu.write_to_oam_addr(0x10);
    //     ppu.write_to_oam_addr(0x77);
    //     ppu.write_to_oam_addr(0x11);
    //     ppu.write_to_oam_addr(0x66);
    // }
}
