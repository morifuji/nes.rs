pub struct Frame {
   pub data: Vec<u8>
}


impl Frame {
   // フレームとして画面に表示される範囲
   const WIDTH: usize = 256;
   // フレームとして画面に表示される範囲
   const HEIGHT: usize = 240;

   pub fn new() -> Self {
      Self { data: vec![0; Frame::WIDTH * Frame::HEIGHT * 3] }
   }

   pub fn set_pixel(&mut self, x: usize, y: usize, rgb: (u8,u8,u8)) {
      let base_address = y*Frame::WIDTH * 3 + x * 3;
      // WHY?
      if base_address + 2 >= self.data.len() {
         //_log::debug!("address is out!");
         return;
      }
      self.data[base_address] = rgb.0;
      self.data[base_address+1] = rgb.1;
      self.data[base_address+2] = rgb.2;
   }

}